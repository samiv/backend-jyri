const router = require("express").Router();
let Costa = require("../models/costa.model");

router.route("/").get((req, res) => {
  Costa.find()
    .then(costas => res.json(costas))
    .catch(err => res.status(400).json("Error: " + err));
});

router.route("/add").post((req, res) => {
  const name = req.body.name;
  const organizer = req.body.organizer;
  const location = req.body.location;
  const startDate = Date.parse(req.body.startDate);
  const endDate = Date.parse(req.body.endDate);
  const description = req.body.description;

  const newCosta = new Costa({
    name,
    startDate,
    endDate,
    location,
    description,
    organizer
  });

  newCosta
    .save()
    .then(() => res.json("Costa added!"))
    .catch(err => res.status(400).json("Error: " + err));
});

router.route("/:id").delete((req, res) => {
  Costa.findByIdAndDelete(req.params.id)
    .then(() => res.json("Costa deleted."))
    .catch(err => res.status(400).json("Error: " + err));
});

router.route("/update/:id").post((req, res) => {
  Costa.findById(req.params.id)
    .then(costa => {
      costa.name = req.body.name;
      costa.organizer = req.body.organizer;
      costa.location = req.body.location;
      costa.startDate = Date.parse(req.body.startDate);
      costa.endDate = Date.parse(req.body.endDate);
      costa.description = req.body.description;

      costa.save()
        .then(() => res.json("Costa Updated!"))
        .catch(err => res.status(400).json("Error: " + err));
    })
    .catch(err => res.status(400).json("Error: " + err));
});

module.exports = router;
