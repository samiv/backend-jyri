const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const costaSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    startDate: {
      type: Date
    },
    endDate: {
      type: Date
    },
    location: {
      type: String
    },
    description: String,
    organizer: String
  },
  { timestamps: true }
);

const Costa = mongoose.model("Costa", costaSchema);

module.exports = Costa;
